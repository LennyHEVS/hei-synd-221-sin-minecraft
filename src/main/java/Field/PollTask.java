package Field;

import Core.BooleanDataPoint;
import Core.DPType;
import Core.FloatingDataPoint;
import SmartControl.SmartControl;
import Web.WebConnector;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class performs a single read of all the registers and dies
 * This class is a TimerTask instance
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class PollTask extends TimerTask {

    /**
     * Performs a single read of all the registers and dies
     */
    public void run(){
        FloatingRegister.poll();
        BooleanRegister.poll();
    }

    /**
     * The main function is used to test the PollTask class
     */
    public static void main(String args[]){
        WebConnector.getInstance().start();

        //Creates 4 registers and performs a poll every 5 seconds
        //FloatingRegister frIn = new FloatingRegister(89,new FloatingDataPoint("GRID_U_FLOAT", DPType.INPUT,0f));            //OK
        //FloatingRegister frOut = new FloatingRegister(205,new FloatingDataPoint("REMOTE_FACTORY_SP", DPType.OUTPUT,0f));  //OK
        //BooleanRegister brIn = new BooleanRegister(609,new BooleanDataPoint("SOLAR_CONNECT_SW", DPType.INPUT,true));        //OK
        //BooleanRegister brOut = new BooleanRegister(405,new BooleanDataPoint("REMOTE_WIND_SW", DPType.OUTPUT,true));        //OK

        //Test DataBaseConnector
        FloatingRegister GridVoltage = new FloatingRegister(89,new FloatingDataPoint("GRID_U_FLOAT", DPType.INPUT,0f));
        FloatingRegister BatteryCharge = new FloatingRegister(49,new FloatingDataPoint("BATT_CHRG_FLOAT", DPType.INPUT,0f));
        FloatingRegister BatteryPower = new FloatingRegister(57,new FloatingDataPoint("BATT_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister SolarPower = new FloatingRegister(61,new FloatingDataPoint("SOLAR_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister WindPower = new FloatingRegister(53,new FloatingDataPoint("WIND_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister CoalPower = new FloatingRegister(81,new FloatingDataPoint("COAL_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister CoalAmount = new FloatingRegister(65,new FloatingDataPoint("COAL_AMOUNT", DPType.INPUT,0f));
        FloatingRegister HomePower = new FloatingRegister(101,new FloatingDataPoint("HOME_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister StreetPower = new FloatingRegister(97,new FloatingDataPoint("PUBLIC_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister FactoryPower = new FloatingRegister(105,new FloatingDataPoint("FACTORY_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister BunkerPower = new FloatingRegister(93,new FloatingDataPoint("BUNKER_P_FLOAT", DPType.INPUT,0f));
        FloatingRegister WindStrength = new FloatingRegister(301,new FloatingDataPoint("WIND_FLOAT", DPType.INPUT,0f));
        FloatingRegister Weather = new FloatingRegister(305,new FloatingDataPoint("WEATHER_FLOAT", DPType.INPUT,0f));
        FloatingRegister WeatherForecast = new FloatingRegister(309,new FloatingDataPoint("WEATHER_FORECAST_FLOAT", DPType.INPUT,0f));
        FloatingRegister WeatherCountdown = new FloatingRegister(313,new FloatingDataPoint("WEATHER_COUNTDOWN_FLOAT", DPType.INPUT,0f));
        FloatingRegister Clock = new FloatingRegister(317,new FloatingDataPoint("CLOCK_FLOAT", DPType.INPUT,0f));
        FloatingRegister RemoteCoalSetPoint = new FloatingRegister(209,new FloatingDataPoint("REMOTE_COAL_SP", DPType.OUTPUT,0f));
        FloatingRegister RemoteFactorySetPoint = new FloatingRegister(205,new FloatingDataPoint("REMOTE_FACTORY_SP", DPType.OUTPUT,0f));
        BooleanRegister SolarSwitch = new BooleanRegister(401,new BooleanDataPoint("REMOTE_SOLAR_SW", DPType.OUTPUT,false));
        BooleanRegister WindSwitch = new BooleanRegister(405,new BooleanDataPoint("REMOTE_WIND_SW", DPType.OUTPUT,false));
        FloatingRegister FactoryEnergy = new FloatingRegister(341,new FloatingDataPoint("FACTORY_ENERGY", DPType.INPUT,0f));
        FloatingRegister Score = new FloatingRegister(345,new FloatingDataPoint("SCORE", DPType.INPUT,0f));
        FloatingRegister CoalSetPoint = new FloatingRegister(601,new FloatingDataPoint("COAL_SP", DPType.INPUT,0f));
        FloatingRegister FactorySetPoint = new FloatingRegister(605,new FloatingDataPoint("FACTORY_SP", DPType.INPUT,0f));
        BooleanRegister SolarConnectSwitch = new BooleanRegister(609,new BooleanDataPoint("SOLAR_CONNECT_SW", DPType.INPUT,false));
        BooleanRegister WindConnectSwitch = new BooleanRegister(613,new BooleanDataPoint("WIND_CONNECT_SW", DPType.INPUT,false));

        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(),0,1000);
        SmartControl.getInstance();
    }
}
