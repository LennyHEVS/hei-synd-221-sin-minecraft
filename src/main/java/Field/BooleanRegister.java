package Field;

import Core.BooleanDataPoint;

import java.util.HashMap;

/**
 * This class represents a boolean register that is a digital twin of a register contained in the minecraft server
 * This class is an instance of Register
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class BooleanRegister extends Register {
    /**
     * the Datapoint containing the register value and label
     */
    private BooleanDataPoint datapoint;
    private static HashMap<BooleanDataPoint, BooleanRegister> map = new HashMap<>();    //This map is used to get the right BooleanRegister with only knowing its BooleanDataPoint

    /**
     * Constructs a BooleanRegister with the given address and BooleanDataPoint
     *
     * @param addr its address in the minecraft server
     * @param bdp its BooleanDataPoint containing its value and label
     */
    public BooleanRegister(int addr, BooleanDataPoint bdp){
        super(addr);
        datapoint = bdp;
        map.put(datapoint,this);
    }
    /**
     * Reads the value of this register in the minecraft server and holds it
     */
    public void read() {
        boolean newValue = ModbusAccessor.getInstance().readBool(address);
        datapoint.setNewValue(newValue);
    }
    /**
     * Write the current value of this register in the minecraft server
     */
    public  void write() {
        boolean value = datapoint.getValue();
        ModbusAccessor.getInstance().writeBool(address,value);
    }

    /**
     * Return the BooleanRegister containing the given BooleanDataPoint
     *
     * @param dp the BooleanDataPoint
     * @return the corresponding BooleanRegister
     */
    public static BooleanRegister getRegisterFromDataPoint(BooleanDataPoint dp){
        return (map.get(dp));
    }

    /**
     * Static method
     * Performs a single read on every BooleanDataPoint
     */
    public static void poll(){
        map.forEach((dp, reg) -> {
            reg.read();
        });
    }
}
