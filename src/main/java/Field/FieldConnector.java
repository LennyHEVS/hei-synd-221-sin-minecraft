package Field;
import Core.BooleanDataPoint;
import Core.FloatingDataPoint;

/**
 * This class handles the communication with the Field. This is a singleton class.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class FieldConnector {
    // Calling private constructor and assigning instance to static variable fc
    /**
     *  Unique FieldConnector of the software (Singleton pattern)
     */
    private static FieldConnector fc = new FieldConnector();

    // Private constructor
    private FieldConnector(){

    }

    /**
     * This method is called when a BooleanDataPoint has its value changed
     *
     * @param p the BooleanDataPoint that has its value changed
     */
    public void onNewValueBool(BooleanDataPoint p){
        String label;
        boolean value;
        label = p.getLabel();
        value = p.getValue();

        pushToFieldBool(label,value);
    }

    /**
     * This method is called when a FloatingDataPoint has its value changed
     *
     * @param p the FloatingDataPoint that has its value changed
     */
    public void onNewValueFloat(FloatingDataPoint p){
        String label;
        float value;
        label = p.getLabel();
        value = p.getValue();

        pushToFieldFloat(label,value);
    }

    /**
     * Push the new value of the boolean point with the given name to the Field
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToFieldBool(String label, boolean value){
        //System.out.println("Push to Field " + label + " with value " + value);

        BooleanRegister br = BooleanRegister.getRegisterFromDataPoint((BooleanDataPoint) BooleanDataPoint.getDataPointFromLabel(label));
        br.write();
    }

    /**
     * Push the new value of the floating point with the given name to the Field
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToFieldFloat(String label, float value){
        //System.out.println("Push to Field " + label + " with value " + value);
        FloatingRegister fr = FloatingRegister.getRegisterFromDataPoint((FloatingDataPoint)FloatingDataPoint.getDataPointFromLabel(label));
        fr.write();
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return a reference to the singleton
     */
    public static FieldConnector getInstance() {
        return fc;

    }
}
