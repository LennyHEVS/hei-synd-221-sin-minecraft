package Field;

import javafx.util.Pair;
import org.apache.commons.lang3.ObjectUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;

/**
 * This class handles the communication with minecraft world through a modbus over TCP connection. This is a singleton class.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class ModbusAccessor extends Socket{
    /**
     * The server port of the minecraft modbus over TCP server
     */
    public static final int		SERVER_PORT			= 1502;
    /**
     * The modbus RTU address of the minecraft server
     */
    public static final int     RTU_ADDRESS         = 1;

    private static short i = 0; //Transaction identifier

    private static HashMap<Short, Pair<Integer,Integer>> map = new HashMap<>();    //This map is used to find the offset and range of the datapoint at the given address

    /**
     *  Unique ModbusAccessor of the software (Singleton pattern)
     */
    private static ModbusAccessor ma;
    static {
        try {
            ma = new ModbusAccessor(InetAddress.getLocalHost(),SERVER_PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Default constructor
     *
     * @throws Exception
     */
    public ModbusAccessor(InetAddress addr, int port) throws Exception{                           //Connects to the minecraft server and creates the map with the register addresses, range and offset
        super(addr, port);

        map.put((short)89,new Pair<Integer,Integer>(1000,0));
        map.put((short)57,new Pair<Integer,Integer>(6000,-3000));
        map.put((short)49,new Pair<Integer,Integer>(1,0));
        map.put((short)61,new Pair<Integer,Integer>(1500,0));
        map.put((short)53,new Pair<Integer,Integer>(1000,0));
        map.put((short)81,new Pair<Integer,Integer>(600,0));
        map.put((short)65,new Pair<Integer,Integer>(1,0));
        map.put((short)101,new Pair<Integer,Integer>(1000,0));
        map.put((short)97,new Pair<Integer,Integer>(500,0));
        map.put((short)105,new Pair<Integer,Integer>(2000,0));
        map.put((short)93,new Pair<Integer,Integer>(500,0));
        map.put((short)301,new Pair<Integer,Integer>(1,0));
        map.put((short)305,new Pair<Integer,Integer>(1,0));
        map.put((short)309,new Pair<Integer,Integer>(1,0));
        map.put((short)313,new Pair<Integer,Integer>(600,0));
        map.put((short)317,new Pair<Integer,Integer>(1,0));
        map.put((short)209,new Pair<Integer,Integer>(1,0));
        map.put((short)205,new Pair<Integer,Integer>(1,0));
        map.put((short)401,new Pair<Integer,Integer>(1,0));
        map.put((short)405,new Pair<Integer,Integer>(1,0));
        map.put((short)341,new Pair<Integer,Integer>(3600000,0));
        map.put((short)345,new Pair<Integer,Integer>(3600000,0));
        map.put((short)601,new Pair<Integer,Integer>(1,0));
        map.put((short)605,new Pair<Integer,Integer>(1,0));
        map.put((short)609,new Pair<Integer,Integer>(1,0));
        map.put((short)613,new Pair<Integer,Integer>(1,0));
    }

    /**
     * Read the float value of the register at the address regAddress in the minecraft world and return it
     *
     * @param regAddress address of the register
     * @return its current value
     */
    public float readFloat(int regAddress){
        ByteBuffer theBB = ByteBuffer.allocate(12);
        byte[] arr;

        theBB.putShort((short)i);           //Transaction identifier
        theBB.putShort((short)0);           //Protocol identifier
        theBB.putShort((short)6);           //Length
        theBB.put((byte)RTU_ADDRESS);       //RTU address
        theBB.put((byte)3);                 //function code
        theBB.putShort((short)regAddress);  //register address
        theBB.putShort((short)2);           //Quantity of registers
        arr = theBB.array();

        send(arr);

        try {
            InputStream is = ma.getInputStream();

            arr = new byte[13];
            is.read(arr, 0, 13);
            while(is.available()>0)
            {
                is.read();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        theBB = ByteBuffer.wrap(arr);
        i++;
        try {
            if(arr[7]==3) {
                return theBB.getFloat(9);
            }
            else
            {
                return Float.MAX_VALUE;
            }
        }
        catch(Exception e){
            System.out.println(theBB);
            return Float.MAX_VALUE;
        }
    }
    /**
     * Read the boolean value of the register at the address regAddress in the minecraft world and return it
     *
     * @param regAddress address of the register
     * @return its current value
     */
    public boolean readBool(int regAddress){
        float f = readFloat(regAddress);
        if(f==0)
        {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Write the float value newValue in the register at the address regAddress in the minecraft world
     *
     * @param regAddress address of the register
     * @param newValue the value to be written
     */
    public void writeFloat(int regAddress, float newValue){
        ByteBuffer theBB = ByteBuffer.allocate(17);
        byte[] arr;

        theBB.putShort((short)i);           //Transaction identifier
        theBB.putShort((short)0);           //Protocol identifier
        theBB.putShort((short)11);          //Length
        theBB.put((byte)RTU_ADDRESS);       //RTU address
        theBB.put((byte)16);                //function code
        theBB.putShort((short)regAddress);  //register address
        theBB.putShort((short)2);           //Quantity of registers
        theBB.put((byte)4);                 //byte count
        theBB.putFloat(newValue);             //value
        arr = theBB.array();

        send(arr);

        try {
            arr = new byte[6];
            ma.getInputStream().read(arr, 0, 6);    //Do we need to test errors ?
            arr = new byte[arr[5]];
            ma.getInputStream().read(arr, 0, arr.length);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        i++;
    }

    /**
     * Write the boolean value newValue in the register at the address regAddress in the minecraft world
     *
     * @param regAddress address of the register
     * @param newValue the value to be written
     */
    public void writeBool(int regAddress, boolean newValue){
        float newValuef;
        if(newValue == false)
        {
            newValuef = 0f;
        }
        else
        {
            newValuef = 1f;
        }
        writeFloat(regAddress, newValuef);
    }

    /**
     * Send the given array via the modbus connection over TCP
     *
     * @param array the array to be sent
     */
    private void send(byte[] array){
        try{
            OutputStream out = ma.getOutputStream();
            out.write(array);
            out.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Translate the float value with range 0 to 1 to its real value using the map containing the range and the offset of each register
     *
     * @param regAdress address of the register
     * @param value the value with range 0 to 1
     * @return the real value of the register
     */
    public static float translateToModbus(int regAdress, float value)
    {
        int offset = map.get((short)regAdress).getValue();
        int range = map.get((short)regAdress).getKey();

        return ((value - offset) / range);
    }

    /**
     * Translate the float value with its own range and offset to a value with range 0 to 1 ready to be sent over modbus
     *
     * @param regAdress address of the register
     * @param value the real value of the register
     * @return the value with range 0 to 1
     */
    public static float translateToData(int regAdress, float value)
    {
        int offset = map.get((short)regAdress).getValue();
        int range = map.get((short)regAdress).getKey();

        return (value * range  + offset);
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return a reference to the singleton
     */
    public static ModbusAccessor getInstance() {
        return ma;

    }

    /**
     * The main function is used to test the ModbusAccessor class
     */
    public static void main(String args[]){
        float GridVolt = ma.readFloat(89);
        System.out.println("Grid Voltage = " + translateToData(89,GridVolt) +"V");

        float PowerBat = ma.readFloat(57);
        System.out.println("Power drawn(+)/stored(-) from battery = " + translateToData(57,PowerBat) +"W");

        System.out.println(ma.readBool(401));
        ma.writeBool(401,true);
        System.out.println(ma.readBool(401));
    }
}