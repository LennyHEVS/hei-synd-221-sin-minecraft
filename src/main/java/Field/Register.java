package Field;


/**
 * This abstract class represents a register that is a digital twin of a register contained in the minecraft server
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public abstract class Register {
    /**
     * the address of the register in the minecraft server
     */
    protected int address;

    /**
     * Constructs a register with the given address
     *
     * @param addr its address in the minecraft server
     */
    public Register(int addr) {
        address = addr;
    }

    /**
     * Abstract method
     * Reads the value of this register in the minecraft server and holds it
     */
    public abstract void read();
    /**
     * Abstract method
     * Write the current value of this register in the minecraft server
     */
    public abstract void write();
}
