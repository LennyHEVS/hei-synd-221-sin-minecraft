package Field;

import Core.FloatingDataPoint;

import java.util.HashMap;

/**
 * This class represents a float register that is a digital twin of a register contained in the minecraft server
 * This class is an instance of Register
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class FloatingRegister extends Register {
    /**
     * the Datapoint containing the register value and label
     */
    private FloatingDataPoint datapoint;
    private static HashMap<FloatingDataPoint, FloatingRegister> map = new HashMap<>();  //This map is used to get the right FloatingRegister with only knowing its FloatingDataPoint

    /**
     * Constructs a FloatingRegister with the given address and FloatingDataPoint
     *
     * @param addr its address in the minecraft server
     * @param fdp its FloatingDataPoint containing its value and label
     */
    public FloatingRegister(int addr, FloatingDataPoint fdp){
        super(addr);
        datapoint = fdp;
        map.put(datapoint,this);
    }
    /**
     * Reads the value of this register in the minecraft server and holds it
     */
    public void read() {
        float newValue = ModbusAccessor.getInstance().readFloat(address);
        if(newValue!=Float.MAX_VALUE)
        {
            newValue = ModbusAccessor.translateToData(address,newValue);
            datapoint.setNewValue(newValue);
        }
    }
    /**
     * Write the current value of this register in the minecraft server
     */
    public  void write() {
        float value = datapoint.getValue();
        ModbusAccessor.getInstance().writeFloat(address,value);
    }
    /**
     * Return the FloatingRegister containing the given FloatingDataPoint
     *
     * @param dp the FloatingDataPoint
     * @return the corresponding FloatingRegister
     */
    public static FloatingRegister getRegisterFromDataPoint(FloatingDataPoint dp){
        return (map.get(dp));
    }
    /**
     * Static method
     * Performs a single read on every BooleanDataPoint
     */
    public static void poll(){
        map.forEach((dp, reg) -> {
            reg.read();
        });
    }
}
