package Web;
import Core.BooleanDataPoint;
import Core.DataPoint;
import Core.FloatingDataPoint;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.Vector;

/**
 * This class handles the communication with the Web pages. This is a singleton class.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class WebConnector extends WebSocketServer {
    /**
     * Web pages port number
     */
    private static int port = 8888;

    // Calling private constructor and assigning instance to static variable wc
    /**
     *  Unique WebConnector of the software (Singleton pattern)
     */
    private static WebConnector wc;
    static {
        try {
            wc = new WebConnector();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A vector containing all connected web pages
     */
    private Vector<WebSocket> webPages = new Vector();

    // Private constructor
    private WebConnector() throws Exception{
        super(new InetSocketAddress(port));
    }

    /**
     * Callback method called when a web page first connects to the WebConnector web server
     * Initialize all the datapoints to their current values
     *
     * @param webSocket The websocket of the web page
     * @param clientHandshake The handshake of the client web page
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        webPages.add(webSocket);

        webSocket.send("Connected to the SIn07 world !");

        FloatingDataPoint FlDp;
        BooleanDataPoint BlDp;

        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("COAL_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("GRID_U_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("HOME_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("WIND_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("COAL_AMOUNT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("BATT_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("COAL_SP");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("FACTORY_SP");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());

        BlDp = (BooleanDataPoint)DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        webSocket.send(BlDp.getLabel() + "=" + (BlDp.getValue()?"true":"false"));
        BlDp = (BooleanDataPoint)DataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        webSocket.send(BlDp.getLabel() + "=" + (BlDp.getValue()?"true":"false"));
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
        FlDp = (FloatingDataPoint)DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        webSocket.send(FlDp.getLabel() + "=" + FlDp.getValue());
    }

    /**
     * Callback method called when a web page closes
     *
     * @param webSocket The websocket of the web page
     * @param i The close frame code
     * @param s The reason for closing
     * @param b Returns whether or not the closing of the connection was initiated by the remote host
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        webPages.remove(webSocket);
    }

    /**
     * Callback method called when we receive a message from a web page
     *
     * @param webSocket The websocket of the web page
     * @param s The sent message
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String[] splitString = s.split("=");
        String label = splitString[0];
        String value = splitString[1];

        if(label.equals("REMOTE_SOLAR_SW") || label.equals("REMOTE_WIND_SW"))
        {
            BooleanDataPoint dp = (BooleanDataPoint) DataPoint.getDataPointFromLabel(label);
            dp.setNewValue(value.equals("false")?false:true);
        }
        else if (label.equals("REMOTE_COAL_SP") || label.equals("REMOTE_FACTORY_SP"))
        {
            FloatingDataPoint dp  = (FloatingDataPoint) DataPoint.getDataPointFromLabel(label);
            dp.setNewValue(Float.parseFloat(value));
        }
    }

    /**
     * Callback method called when an error as occured
     *
     * @param webSocket The websocket of the web page
     * @param e The occured exception
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    /**
     * Callback method called when we first start the web server
     *
     */
    @Override
    public void onStart() {
        boolean a = true;
    }

    /**
     * This method is called when a BooleanDataPoint has its value changed
     *
     * @param p the BooleanDataPoint that has its value changed
     */
    public void onNewValueBool(BooleanDataPoint p){
        String label;
        boolean value;
        label = p.getLabel();
        value = p.getValue();

        pushToWebPagesBool(label,value);
    }

    /**
     * This method is called when a FloatingDataPoint has its value changed
     *
     * @param p the FloatingDataPoint that has its value changed
     */
    public void onNewValueFloat(FloatingDataPoint p){
        String label;
        float value;
        label = p.getLabel();
        value = p.getValue();

        pushToWebPagesFloat(label,value);
    }

    /**
     * Push the new value of the boolean point with the given name to the Web pages
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToWebPagesBool(String label, boolean value){
        //System.out.println("Push to Web " + label + " with value " + value);

        for(WebSocket socket : webPages)
        {
            socket.send(label + "=" + (value?"true":"false"));
        }
    }

    /**
     * Push the new value of the floating point with the given name to the Web pages
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToWebPagesFloat(String label, float value){
        //System.out.println("Push to Web " + label + " with value " + value);


        for(WebSocket socket : webPages)
        {
            socket.send(label + "=" + value);
        }
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return a reference to the singleton
     */
    public static WebConnector getInstance() {
        return wc;

    }
}
