package SmartControl;


import Core.BooleanDataPoint;
import Core.FloatingDataPoint;
import com.sun.deploy.security.SelectableSecurityManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is used to control the system. This is a singleton class.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class SmartControl {
    // Calling private constructor and assigning instance to static variable sc
    /**
     *  Unique SmartControl of the software (Singleton pattern)
     */
    private static SmartControl sc = new SmartControl();

    /**
     * The timer task is only calling the function doControl
     */
    TimerTask contrTask = new TimerTask() {
        @Override
        public void run() {
            sc.doControl();
        }
    };
    // Private constructor
    private SmartControl(){
        Timer contrTimer = new Timer();
        contrTimer.scheduleAtFixedRate(contrTask,0,1000);

    }

    /**
     * Method that regulates our world switches and set points to have the best score after 2.5 days
     */
    public void doControl(){
        FloatingDataPoint REMOTE_FACTORY_SP = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        BooleanDataPoint REMOTE_SOLAR_SW = (BooleanDataPoint) BooleanDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        BooleanDataPoint REMOTE_WIND_SW = (BooleanDataPoint) BooleanDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        FloatingDataPoint REMOTE_COAL_SP = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        FloatingDataPoint BATT_CHRG_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatingDataPoint SOLAR_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
        FloatingDataPoint SCORE = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("SCORE");
        FloatingDataPoint CLOCK_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("CLOCK_FLOAT");
        FloatingDataPoint HOME_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("HOME_P_FLOAT");
        FloatingDataPoint FACTORY_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");
        FloatingDataPoint BUNKER_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
        FloatingDataPoint PUBLIC_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
        FloatingDataPoint WIND_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("WIND_P_FLOAT");
        FloatingDataPoint COAL_P_FLOAT = (FloatingDataPoint) FloatingDataPoint.getDataPointFromLabel("COAL_P_FLOAT");

        REMOTE_WIND_SW.setNewValue(true);
        REMOTE_SOLAR_SW.setNewValue(true);
        System.out.println("Score reached until now: " + SCORE.getValue());
        System.out.println("Time of day: " + CLOCK_FLOAT.getValue()*24);


        if(CLOCK_FLOAT.getValue()<0.23f || CLOCK_FLOAT.getValue()>0.73f) {      //AT NIGHT
            REMOTE_FACTORY_SP.setNewValue(0.4f);                                //Factory always at 40%
            System.out.println("Smart Control : Set the coal set point to 40%");
            if(BATT_CHRG_FLOAT.getValue()<0.5f) {                               //if the batteries are under 50% -> coal set point at 100%
                REMOTE_COAL_SP.setNewValue(1f);
                System.out.println("Smart Control : Set the coal set point to 100%");
            }
            else if(BATT_CHRG_FLOAT.getValue()<0.7f)                            //if the batteries are between 50% and 70% -> regulation
            {
                float CoalSP = (HOME_P_FLOAT.getValue() + FACTORY_P_FLOAT.getValue() + PUBLIC_P_FLOAT.getValue() + BUNKER_P_FLOAT.getValue() - WIND_P_FLOAT.getValue() - SOLAR_P_FLOAT.getValue())/600f;
                if(CoalSP>0f)
                {
                    if(CoalSP<1f) {                             //restricts the set point between 0f and 1f
                        REMOTE_COAL_SP.setNewValue(CoalSP);
                        System.out.println("Smart Control : Set the coal set point to " + CoalSP * 100 + "%");
                    }
                    else
                    {
                        REMOTE_COAL_SP.setNewValue(1f);
                        System.out.println("Smart Control : Set the coal set point to 100%");
                    }
                }
                else
                {
                    REMOTE_COAL_SP.setNewValue(0f);
                    System.out.println("Smart Control : Set the coal set point to 0%");
                }
            }
            else                                        //if the batteries are above 70% -> shut off the coal and factory at 100%
            {
                REMOTE_COAL_SP.setNewValue(0f);
                System.out.println("Smart Control : Set the coal set point to 0%");
                REMOTE_FACTORY_SP.setNewValue(1f);
                System.out.println("Smart Control : Set the factory set point to 100%");

            }
        }
        else                                        //DURING THE DAY
        {
            if(REMOTE_COAL_SP.getValue()>0f) {      //no coal needed
                REMOTE_COAL_SP.setNewValue(0f);
                System.out.println("Smart Control : Set the coal set point to 0%");
            }
            if(BATT_CHRG_FLOAT.getValue()>0.85f) {  //if the batteries are above 85% -> factory set point at 100%
                REMOTE_FACTORY_SP.setNewValue(1f);
                System.out.println("Smart Control : Set the factory set point to 100%");
            }
            else if(BATT_CHRG_FLOAT.getValue()>0.5f)    //if the batteries are between 50% and 85% -> regulation
            {
                float FactorySP = (COAL_P_FLOAT.getValue() + SOLAR_P_FLOAT.getValue() + WIND_P_FLOAT.getValue() - HOME_P_FLOAT.getValue() - PUBLIC_P_FLOAT.getValue() - BUNKER_P_FLOAT.getValue())/1000f;
                if(BATT_CHRG_FLOAT.getValue()<0.6f)    //if the batteries are between 50% and 60% -> lower the factory by 5%
                {
                    FactorySP = FactorySP - 0.05f;
                }
                if(FactorySP>0f)                        //restricts the set point between 0f and 1f
                {
                    if(FactorySP<1f) {
                        REMOTE_FACTORY_SP.setNewValue(FactorySP);
                        System.out.println("Smart Control : Set the factory set point to " + FactorySP * 100 + "%");
                    }
                    else
                    {
                        REMOTE_FACTORY_SP.setNewValue(1f);
                        System.out.println("Smart Control : Set the factory set point to 100%");
                    }
                }
                else
                {
                    REMOTE_FACTORY_SP.setNewValue(0f);
                    System.out.println("Smart Control : Set the factory set point to 0%");
                }
            }
            else                    //if the batteries are lower than 50% -> shut off the factory
            {
                REMOTE_FACTORY_SP.setNewValue(0f);
                System.out.println("Smart Control : Set the factory set point to 0%");
            }
        }
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return a reference to the singleton
     */
    public static SmartControl getInstance() {
        return sc;
    }

}
