package Database;
import Core.BooleanDataPoint;
import Core.FloatingDataPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import javax.net.ssl.HttpsURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

/**
 * This class handles the communication with the DataBase. This is a singleton class.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class DatabaseConnector {
    /**
     *  User name and password to access the database
     */
    private static String userpass = "SIn07:5cc836ddef7ed878ab8c5a373e4dd6b5";

    // Calling private constructor and assigning instance to static variable dbc
    /**
     *  Unique DataBaseConnector of the software (Singleton pattern)
     */
    private static DatabaseConnector dbc = new DatabaseConnector();

    /**
     *  The url of the database
     */
    private static URL url;
    static {
        try {
            url = new URL("https://influx.sdi.hevs.ch/write?db=SIn07");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Private constructor
    private DatabaseConnector(){

    }

    /**
     * Method used to set the user name and password of the database
     *
     * @param user the user string
     * @param password the password string
     */
    public static void setUserpass(String user, String password) {
        DatabaseConnector.userpass = user + ":" + password;
    }

    /**
     * Method used to set the url of the database
     *
     * @param theurl the database url
     */
    public static void setUrl(String theurl) throws MalformedURLException
    {
        url = new URL(theurl);
    }

    /**
     * This method is called when a BooleanDataPoint has its value changed
     *
     * @param p the BooleanDataPoint that has its value changed
     */
    public void onNewValueBool(BooleanDataPoint p){
        String label;
        boolean value;
        label = p.getLabel();
        value = p.getValue();

        try {
            pushToDBBool(label, value);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method is called when a FloatingDataPoint has its value changed
     *
     * @param p the FloatingDataPoint that has its value changed
     */
    public void onNewValueFloat(FloatingDataPoint p){
        String label;
        float value;
        label = p.getLabel();
        value = p.getValue();

        try {
            pushToDBFloat(label, value);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Push the new value of the boolean point with the given name to the DataBase
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToDBBool(String label, boolean value) throws IOException {
        //System.out.println("Push to Database " + label + " with value " + value);

        String body = label  + " value=" + (value?"1.0" : "0.0");

        send(body);
    }
    /**
     * Push the new value of the floating point with the given name to the DataBase
     *
     * @param label its name
     * @param value the value of the point
     */
    private void pushToDBFloat(String label, float value) throws IOException{
        //System.out.println("Push to Database " + label + " with value " + value);

        String body = label  + " value=" + Float.toString(value);

        send(body);
    }

    /**
     * Sends an HTTP POST to the database with the given body
     *
     * @param body the message body to be posted
     * @throws IOException
     */
    private void send(String body) throws IOException{
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
        connection.setRequestProperty ("Authorization", "Basic " + encoding);
        connection.setRequestProperty("Content-Type", "binary/octet-stream");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);


        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

        writer.write(body);
        writer.flush();
        //int responseCode = connection.getResponseCode();
        //System.out.println("Code: " + responseCode + "\nMessage: " + connection.getResponseMessage());

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((in.readLine()) != null) {}

        in.close();
        writer.close();
        connection.disconnect();
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return a reference to the singleton
     */
    public static DatabaseConnector getInstance() {
        return dbc;

    }
}
