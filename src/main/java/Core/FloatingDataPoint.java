package Core;

import Database.DatabaseConnector;
import Field.FieldConnector;
import Web.WebConnector;

/**
 * This class holds a floating value and a type (input/output). It can be found by its name.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class FloatingDataPoint extends DataPoint {
    private float value;

    /**
     * Creates a FloatingDataPoint with the given parameters
     *
     * @param l its name
     * @param t type, either it's an input or an output
     * @param v holding value
     */
    public FloatingDataPoint(String l, DPType t, float v){
        super(l,t);
        value = v;
    }

    /**
     * Assign the given value to the DataPoint
     *
     * @param newValue the new value you want to write
     */
    @Override
    public void setNewValue(Object newValue){
        value = (Float) newValue;
        DatabaseConnector.getInstance().onNewValueFloat(this);   //Update the database
        WebConnector.getInstance().onNewValueFloat(this);        //Update the Web pages
        if(type==DPType.OUTPUT) {                                   //Update the Field if the DataPoint is an output
            FieldConnector.getInstance().onNewValueFloat(this); //
        }
    }

    /**
     * Returns the holding value of the DataPoint
     *
     * @return its holding value
     */
    public float getValue(){
        return value;
    }
}
