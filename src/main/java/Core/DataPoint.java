package Core;
import java.util.HashMap;

/**
 * This class is the base class for the two subclass BooleanDataPoint and FloatingDataPoint
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public abstract class DataPoint {
    /**
     * The name of the datapoint
     */
    protected String label;
    /**
     * The type of the datapoint either INPUT or OUTPUT
     */
    protected DPType type;
    private static HashMap<String, DataPoint> map = new HashMap<>();    //This map is used to get the right DataPoint with only knowing its label

    /**
     * Creates a DataPoint with the given name and type
     *
     * @param l its name
     * @param t type, either it's an input or an output
     */
    public DataPoint(String l, DPType t) {
        label = l;
        type = t;
        map.put(label,this);
    }

    /**
     * Return the type of the DataPoint
     *
     * @return its type
     */
    public DPType getDPType(){
        return type;
    }

    /**
     * Return the name of the DataPoint
     *
     * @return its name
     */
    public String getLabel(){
        return label;
    }

    /**
     * Abstract method for the subclasses. Does nothing.
     *
     * @param newValue the new value you want to write.
     */
    public abstract void setNewValue(Object newValue);

    /**
     * Returns the DataPoint with the given name
     *
     * @param l name of the searched DataPoint
     * @return  the corresponding DataPoint
     */
    public static DataPoint getDataPointFromLabel(String l){
        return (map.get(l));
    }

    /**
     * The main function is used to test the DataPoint class
     */
    public static void main(String args[]){
        BooleanDataPoint p3,p4;
        FloatingDataPoint p5,p6;

        p3 = new BooleanDataPoint("p3", DPType.INPUT,true);
        p4 = new BooleanDataPoint("p4", DPType.OUTPUT,false);
        p5 = new FloatingDataPoint("p5", DPType.INPUT,1.2f);
        p6 = new FloatingDataPoint("p6", DPType.OUTPUT,3.4f);

        System.out.println();

        System.out.println(p3.getLabel()+" with value " + p3.getValue());
        System.out.println(p4.getLabel()+" with value " + p4.getValue());
        System.out.println(p5.getLabel()+" with value " + p5.getValue());
        System.out.println(p6.getLabel()+" with value " + p6.getValue());

        System.out.println();

        p3.setNewValue(false);
        getDataPointFromLabel("p4").setNewValue(true);
        p5.setNewValue(2.3f);
        getDataPointFromLabel("p6").setNewValue(4.5f);

        System.out.println();

        System.out.println(p3.getLabel()+" with value " + p3.getValue());
        System.out.println(p4.getLabel()+" with value " + p4.getValue());
        System.out.println(p5.getLabel()+" with value " + p5.getValue());
        System.out.println(p6.getLabel()+" with value " + p6.getValue());

        DataPoint p = getDataPointFromLabel("p3");
        System.out.println(p.getLabel());
    }
}