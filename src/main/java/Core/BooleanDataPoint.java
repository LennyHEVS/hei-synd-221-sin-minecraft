package Core;


import Database.DatabaseConnector;
import Field.FieldConnector;
import Web.WebConnector;

/**
 * This class holds a boolean value and a type (input/output). It can be found by its name.
 *
 * @author Fumeaux Gaëtan
 * @author Favre Lenny
 */
public class BooleanDataPoint extends DataPoint {
    /**
     * Holding value of the DataPoint
     */
    private boolean value;

    /**
     * Creates a BooleanDataPoint with the given parameters
     *
     * @param l its name
     * @param t type, either it's an input or an output
     * @param v holding value
     */
    public BooleanDataPoint(String l, DPType t, boolean v){
        super(l,t);
        value = v;
    }

    /**
     * Assign the given value to the DataPoint
     *
     * @param newValue the new value you want to write
     */
    @Override
    public void setNewValue(Object newValue){
        value = (Boolean) newValue;
        DatabaseConnector.getInstance().onNewValueBool(this);   //Update the database
        WebConnector.getInstance().onNewValueBool(this);        //Update the Web pages
        if(type==DPType.OUTPUT) {                                   //Update the Field if the DataPoint is an output
            FieldConnector.getInstance().onNewValueBool(this);  //
        }
    }

    /**
     * Returns the holding value of the DataPoint
     *
     * @return its holding value
     */
    public boolean getValue(){
        return value;
    }
}
