package ch.hevs.isi;

import Core.BooleanDataPoint;
import Core.DPType;
import Core.FloatingDataPoint;
import Field.FloatingRegister;
import Field.BooleanRegister;
import Database.DatabaseConnector;
import Field.ModbusAccessor;
import Field.FieldConnector;
import Field.PollTask;
import SmartControl.SmartControl;
import Web.WebConnector;
import ch.hevs.isi.utils.Utility;

import javax.xml.crypto.Data;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.util.Timer;

public class MinecraftController {
    public static FloatingRegister GridVoltage;
    public static FloatingRegister BatteryCharge;
    public static FloatingRegister BatteryPower;
    public static FloatingRegister SolarPower;
    public static FloatingRegister WindPower;
    public static FloatingRegister CoalPower;
    public static FloatingRegister CoalAmount;
    public static FloatingRegister HomePower;
    public static FloatingRegister StreetPower;
    public static FloatingRegister FactoryPower;
    public static FloatingRegister BunkerPower;
    public static FloatingRegister WindStrength;
    public static FloatingRegister Weather;
    public static FloatingRegister WeatherForecast;
    public static FloatingRegister WeatherCountdown;
    public static FloatingRegister Clock;
    public static FloatingRegister RemoteCoalSetPoint;
    public static FloatingRegister RemoteFactorySetPoint;
    public static BooleanRegister SolarSwitch;
    public static BooleanRegister WindSwitch;
    public static FloatingRegister FactoryEnergy;
    public static FloatingRegister Score;
    public static FloatingRegister CoalSetPoint;
    public static FloatingRegister FactorySetPoint;
    public static BooleanRegister SolarConnectSwitch;
    public static BooleanRegister WindConnectSwitch;

    public static boolean USE_MODBUS4J = false;

    /**
     * Shows how to pass the arguments to the program and exits
     */
    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <ModbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    /**
     * This is the main method of our whole program
     * @param args the arguments to be passed
     */
    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        loadDataPoints();
        DatabaseConnector dc = DatabaseConnector.getInstance();
        ModbusAccessor ma = ModbusAccessor.getInstance();

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
            dc.setUserpass(dbUserName,dbPassword);
            try {
                dc.setUrl(dbHostName + "/write?db=" + dbName);
                ma = new ModbusAccessor(InetAddress.getByName(modbusTcpHost), modbusTcpPort);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                usage();
            }

        } else{
            usage();
        }
        FieldConnector fc = FieldConnector.getInstance();
        SmartControl sc = SmartControl.getInstance();
        WebConnector wc = WebConnector.getInstance();
        WebConnector.getInstance().start();
        Timer pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(),0,1000);
    }

    /**
     * This method creates and load all the registers and datapoints needed for this program
     */
    public static void loadDataPoints(){
        GridVoltage = new FloatingRegister(89,new FloatingDataPoint("GRID_U_FLOAT", DPType.INPUT,0f));
        BatteryCharge = new FloatingRegister(49,new FloatingDataPoint("BATT_CHRG_FLOAT", DPType.INPUT,0f));
        BatteryPower = new FloatingRegister(57,new FloatingDataPoint("BATT_P_FLOAT", DPType.INPUT,0f));
        SolarPower = new FloatingRegister(61,new FloatingDataPoint("SOLAR_P_FLOAT", DPType.INPUT,0f));
        WindPower = new FloatingRegister(53,new FloatingDataPoint("WIND_P_FLOAT", DPType.INPUT,0f));
        CoalPower = new FloatingRegister(81,new FloatingDataPoint("COAL_P_FLOAT", DPType.INPUT,0f));
        CoalAmount = new FloatingRegister(65,new FloatingDataPoint("COAL_AMOUNT", DPType.INPUT,0f));
        HomePower = new FloatingRegister(101,new FloatingDataPoint("HOME_P_FLOAT", DPType.INPUT,0f));
        StreetPower = new FloatingRegister(97,new FloatingDataPoint("PUBLIC_P_FLOAT", DPType.INPUT,0f));
        FactoryPower = new FloatingRegister(105,new FloatingDataPoint("FACTORY_P_FLOAT", DPType.INPUT,0f));
        BunkerPower = new FloatingRegister(93,new FloatingDataPoint("BUNKER_P_FLOAT", DPType.INPUT,0f));
        WindStrength = new FloatingRegister(301,new FloatingDataPoint("WIND_FLOAT", DPType.INPUT,0f));
        Weather = new FloatingRegister(305,new FloatingDataPoint("WEATHER_FLOAT", DPType.INPUT,0f));
        WeatherForecast = new FloatingRegister(309,new FloatingDataPoint("WEATHER_FORECAST_FLOAT", DPType.INPUT,0f));
        WeatherCountdown = new FloatingRegister(313,new FloatingDataPoint("WEATHER_COUNTDOWN_FLOAT", DPType.INPUT,0f));
        Clock = new FloatingRegister(317,new FloatingDataPoint("CLOCK_FLOAT", DPType.INPUT,0f));
        RemoteCoalSetPoint = new FloatingRegister(209,new FloatingDataPoint("REMOTE_COAL_SP", DPType.OUTPUT,0f));
        RemoteFactorySetPoint = new FloatingRegister(205,new FloatingDataPoint("REMOTE_FACTORY_SP", DPType.OUTPUT,0f));
        SolarSwitch = new BooleanRegister(401,new BooleanDataPoint("REMOTE_SOLAR_SW", DPType.OUTPUT,false));
        WindSwitch = new BooleanRegister(405,new BooleanDataPoint("REMOTE_WIND_SW", DPType.OUTPUT,false));
        FactoryEnergy = new FloatingRegister(341,new FloatingDataPoint("FACTORY_ENERGY", DPType.INPUT,0f));
        Score = new FloatingRegister(345,new FloatingDataPoint("SCORE", DPType.INPUT,0f));
        CoalSetPoint = new FloatingRegister(601,new FloatingDataPoint("COAL_SP", DPType.INPUT,0f));
        FactorySetPoint = new FloatingRegister(605,new FloatingDataPoint("FACTORY_SP", DPType.INPUT,0f));
        SolarConnectSwitch = new BooleanRegister(609,new BooleanDataPoint("SOLAR_CONNECT_SW", DPType.INPUT,false));
        WindConnectSwitch = new BooleanRegister(613,new BooleanDataPoint("WIND_CONNECT_SW", DPType.INPUT,false));
    }
}
